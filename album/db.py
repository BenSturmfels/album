from pysqlite2 import dbapi2 as sqlite

debug = True

class DB:
    def __init__(self):
        self.database = '/home/ben/album/album.db'
        self.db = sqlite.connect(self.database, isolation_level=None)
        if (self.db == None):
            print "Cannot connect to database %s" % (self.database)
        self.cursor = self.db.cursor()

    def fetch_dict(self, cursor):
        labels = []
        for col in cursor.description:
            labels.append(col[0])

        results = cursor.fetchall()

        results_dict = []
        for row in results:
            results_dict.append(dict(zip(labels, row)))
        return results_dict

    def get_acategories(self):
        query = "SELECT cat_id, title FROM cat ORDER BY cat_id DESC"
        self.cursor.execute(query)
        return self.fetch_dict(self.cursor)

    def get_categories(self):
        query = "SELECT cat_id, title FROM cat ORDER BY cat_id DESC"
        self.cursor.execute(query)
        return self.fetch_dict(self.cursor)

    def get_all_photos(self):
        query = "SELECT filename FROM photo"
        self.cursor.execute(query)
        return self.fetch_dict(self.cursor)

    def get_photos(self, id):
        # put query in execute
        query = """SELECT photo.filename as filename FROM cat NATURAL JOIN in_cat NATURAL JOIN photo WHERE cat.cat_id = ? order by date asc"""
        self.cursor.execute(query, (id,))
        return self.fetch_dict(self.cursor)
        #return result.fetch_row(maxrows=0, how=1)

    def get_photos_by_sql(self, clause):
        query = "SELECT filename FROM photo " + clause
        self.cursor.execute(query)
        return self.fetch_dict(self.cursor)
        #return result.fetch_row(maxrows=0, how=1)

    def get_uncategorised_photos(self):
        # should be where cat_id = NULL - fix after fixed not null entries
        # broken
        #query = """SELECT photo.filename, in_cat.cat_id FROM photo LEFT JOIN in_cat ON photo.filename = in_cat.filename WHERE cat_id is NULL"""

        # put query in execute
        query = """SELECT photo.filename as filename FROM photo
        WHERE uncat = 1 order by date asc"""
        self.cursor.execute(query)
        return self.fetch_dict(self.cursor)
        #return result.fetch_row(maxrows=0, how=1)


    def get_cat_info(self, id):
        # put query in execute
        query = """SELECT title, subtitle, descr
        FROM cat WHERE cat_id = ?"""
        #print query
        self.cursor.execute(query, (id,))
        return self.fetch_dict(self.cursor)
        #return result.fetch_row(maxrows=1, how=1)

    def get_photo_info(self, id):
        self.cursor.execute("""SELECT * FROM photo WHERE filename = ?""", (id,))
        return self.fetch_dict(self.cursor)
        #return result.fetch_row(maxrows=1, how=1)

    def update_photo_info(self, filename, caption, keywords,
                          location, people, photographer, quality,
                          access):
        """Updates the info for a photo given the modified data from
        the interface"""

        self.cursor.execute("""UPDATE photo SET caption = ?,
        keywords = ?, location = ?, people = ?, photographer = ?,
        quality = ?, access = ?
        WHERE filename = ?""",
                            (caption, keywords, location, people,
                             photographer, quality, access,
                             filename))
        # test update was successful??

    def add_category(self, title, subtitle, description):
        """Adds a new category - usually as part of adding a whole
        crapload of photos i've just taken"""
        print title, subtitle, description
        self.cursor.execute("INSERT INTO cat VALUES(NULL, ?, ?, ?)",
                            (title, subtitle, description))
        # test update was successful??

    def add_to_category(self, photoIDs, cat_id):
        # query in execute
        """Add each of a set of photos to a category"""
        for filename in photoIDs:
            query = """INSERT INTO in_cat VALUES(?, ?)"""
            print query
            self.cursor.execute(query, (filename, cat_id))
            query = """UPDATE photo SET uncat=0 WHERE filename=?"""
            print query
            self.cursor.execute(query, (filename,))
            # test success

    def add_photo(self, filename, datetime, width, height, bytes,
                  quality, access, photographer, equipment):
        """Adds a new photo - setting details we can get automatically"""
        ### fix database so these empty strings can be null
        # 1 is to signify the photo is uncategorised
        #print query
        self.cursor.execute("INSERT INTO photo (filename, date, x_dim, y_dim, bytes, access, quality, photographer, equip, uncat) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, 1)",
                            (filename, datetime, width, height, bytes, access, quality, photographer, equipment))
        # test add was successful??

    def update_sizes(self, filename, width, height, bytes):
        """Update after a rotation or resize or similar"""
        self.cursor.execute("""UPDATE photo SET x_dim = ?,
        y_dim = ?, bytes = ? WHERE filename = ?""",
                            (width, height, bytes, filename))
        # test success

    def update_filename(self, oldfilename, newfilename):
        self.cursor.execute("""UPDATE photo SET filename = ?
        WHERE filename = ?""",
                            (newfilename, oldfilename))
        # test success

    def count_photos(self):
        query = "SELECT count(filename) FROM photo"
        if debug:
            print query
        self.cursor.execute(query)
        return self.fetch_dict(self.cursor)
        ##count = result.fetch_row(maxrows=0, how=1)
        #return int(count[0]['count(filename)'])

    def remove_category(self, id):
        print "Removing category", id;
        self.cursor.execute("""DELETE FROM in_cat
        WHERE cat_id = ?""",
                            (id,))
        self.cursor.execute("""DELETE FROM cat
        WHERE cat_id = ?""",
                            (id,))
        #test success

    # HACK for temporary add to export list
    def cat_title_of_photo(self, id):
        self.cursor.execute("""select cat.title from in_cat inner join cat using (cat_id) where filename = ? limit 1""",
                            (id,))
        result = self.fetch_dict(self.cursor)
        return result[0]['title']
