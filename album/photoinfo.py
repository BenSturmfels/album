class PhotoInfo:
	def __init__(self, filename, xdim, ydim, bytes, date,
				 caption, keywords, location, people, photographer, access,
				 quality):
		self.filename = filename
		self.xdim = xdim
		self.ydim = ydim
		self.bytes = bytes
		self.date = date
		self.caption = caption
		self.keywords = keywords
		self.location = location
		self.people = people
		self.photographer = photographer
		self.access = access
		self.quality = quality
