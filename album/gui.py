import os
import string
import thread

import Image
import ImageTk
import tkFileDialog
import tkFont
from Tkinter import (Tk, StringVar, IntVar, Menu, Frame, LEFT, Scrollbar, NO,
                     RIGHT, Y, Listbox, BROWSE, END, Label, EXTENDED, TOP, E, W,
                     NE, Text, WORD, Entry, RIDGE, Radiobutton, Button, ACTIVE)
import tkSimpleDialog

from controller import Album

#rotation degrees - clockwise
LEFT_ROTATION = 270
RIGHT_ROTATION = 90

debug = True

class Gui:
    TITLE = "Ben's Album"
    GEOMETRY_DEFAULT = "700x655"
    thumb_display_height = 200
    thumb_display_width = 200
    thumb_display_filter = Image.ANTIALIAS
    cat_list_length = 35
    photo_list_length = 30
    image_viewer = "display"
    path_image_editor = "gimp-remote"
    listbox_poll_time = 100 # ms
    category_dir = "/home/ben/photos/slideshow"
    slideshow_viewer = "gthumb --fullscreen"

    def __init__(self, parent):
        self.menu_font = tkFont.Font(family="Helvetica", size=11)
        self.list_font = tkFont.Font(family="Helvetica", size=11)
        self.label_font = tkFont.Font(family="Helvetica", size=11, weight="bold")
        # regular font is down the bottom

        self.album = Album();

        self.myParent = parent

        # GUI Variables
        self.curr_date = StringVar()
        self.curr_filename= StringVar()
        self.curr_dims = StringVar()
        self.curr_size = StringVar()
        self.curr_location = StringVar()
        self.curr_people = StringVar()
        self.curr_photographer = StringVar()
        self.curr_quality = IntVar()
        self.curr_access = IntVar()

        self.curr_cat_title = StringVar()
        self.curr_cat_subtitle = StringVar()
        self.curr_cat_desc = StringVar()

        self.initialise_gui()

    # ---------
    # GUI stuff
    # ---------
    def initialise_gui(self):
        # Menubar
        self.menubar = Menu(self.myParent)
        # Add menu
        self.menu_add = Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label="Add", menu=self.menu_add)

        self.menu_add.add_command(label="Add batch of photos...",
                                  command=self.dialog_import_dir_of_photos)
        self.menu_add.add_command(label="Add category...",
                                  command=self.dialog_add_category)

        # Edit menu
        self.menu_edit = Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label="Edit", menu=self.menu_edit)

        self.menu_edit.add_command(label="Rotate left",
                                   command=lambda
                                   : self.rotate(LEFT_ROTATION))
        self.menu_edit.add_command(label="Rotate right",
                                   command=lambda
                                   : self.rotate(RIGHT_ROTATION))
        self.menu_edit.add_command(label="Edit in Gimp",
                                   command=lambda
                                   : self.edit_image())
        self.menu_edit.add_command(label="Update Thumbnail",
                                   command=lambda
                                   : self.update_thumbnail())
        self.menu_edit.add_command(label="Make web gallery",
                                   command=lambda
                                   : self.make_web_gallery())
        self.menu_edit.add_command(label="Export original",
                                   command=lambda
                                   : self.export_original())
        self.menu_edit.add_command(label="Export category",
                                   command=lambda
                                   : self.export_category())
        self.menu_edit.add_command(label="Slideshow",
                                   command=lambda
                                   : self.slideshow())
        self.menu_edit.add_command(label="Get photos by SQL",
                                   command=self.dialog_photos_by_sql)
        self.menu_edit.add_command(label="Get 50 random photos",
                                   command=self.display_50_random_photos)

        # Category menu
        self.menu_category = Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label="Category", menu=self.menu_category)

        self.menu_category.add_command(label="Remove this category",
                                       command=lambda
                                       : self.remove_category())

        # display menu
        self.myParent.config(menu=self.menubar)

        #### Categories window ####
        # look at tixHList if implementing hierachial categories
        self.frame_cat = Frame(self.myParent)
        self.frame_cat.pack(side=LEFT)

        # scrollbar for listbox
        scrollbar = Scrollbar(self.frame_cat, takefocus=NO)
        scrollbar.pack(side=RIGHT, fill=Y)

        #"""export selection allows having items selected in
        #multiple list boxes at once"""
        self.input_cat = Listbox(self.frame_cat,
                                 width=30, height=self.cat_list_length,
                                 selectmode=BROWSE,
                                 exportselection=0,
                                 # inform scrollbar of list events
                                 yscrollcommand=scrollbar.set)

        # inform listbox of scrollbar events
        scrollbar.config(command=self.input_cat.yview)

        # update categories list
        self.display_categories_list(self.input_cat, special=True)

        # focus this widget to start
        self.input_cat.focus_set()

        # Mouse binding to focus the widget
        self.input_cat.bind("<Button-1>",
                            lambda event : self.input_cat.focus_set())

        self.input_cat.pack()

        #### Files pane ####
        self.frame_photos = Frame(self.myParent)
        self.frame_photos.pack(side=LEFT)

        # Category info
        # Title
        self.cat_title = Label(self.frame_photos, text="Title",
                               font=self.label_font)
        self.cat_title.pack()
        # link to instance variable
        self.cat_title["textvariable"] = self.curr_cat_title

        # Subtitle
        self.cat_subtitle = Label(self.frame_photos, text="Subtitle")
        self.cat_subtitle.pack()
        # link to instance variable
        self.cat_subtitle["textvariable"] = self.curr_cat_subtitle

        # Description
        self.cat_desc = Label(self.frame_photos, height=4,
                              wraplength=180, text="Description")
        self.cat_desc.pack()
        # link to instance variable
        self.cat_desc["textvariable"] = self.curr_cat_desc

        ### How many images in this category?

        # scrollbar for listbox
        scrollbar = Scrollbar(self.frame_photos, takefocus=NO)
        scrollbar.pack(side=RIGHT, fill=Y)

        self.input_photos = Listbox(self.frame_photos,
                                    width=23,
                                    height=self.photo_list_length,
                                    selectmode=EXTENDED,
                                    exportselection=0,
                                    yscrollcommand=scrollbar.set)

        scrollbar.config(command=self.input_photos.yview)


        # set current category - used for refresh
        self.current_cat = "uncat"

        # initially display uncategoriesed photos
        self.display_category("uncat")

        # Mouse binding to focus the widget
        self.input_photos.bind("<Button-1>",
                               lambda event :
                                   self.input_photos.focus_set())

        self.input_photos.pack()

        #### THUMB ####
        self.thumb = Label(self.myParent)
        self.thumb.pack(side=TOP)

        # open the photo in a viewer when we click the photo
        self.thumb.bind("<Button-1>",
                        lambda event :
                            thread.start_new_thread(self.view_photo,
                                                    (self.curr_filename.get(),)))


        #### FORM ####
        self.form = Frame(self.myParent)
        self.form.pack(side=TOP)

        # Filename
        self.label_filename = Label(self.form, text="File:",
                                    font=self.label_font)
        self.label_filename.grid(row=0, column=0, sticky=E)
        self.input_filename = Label(self.form,
                                    text="filename",
                                    justify="left" )
        self.input_filename.grid(row=0, column=1, sticky=W)
        # link to filename input to instance variable
        self.input_filename["textvariable"] = self.curr_filename

        # Dimensions & filesize
        self.size = Label(self.form, justify="left")
        self.size.grid(row=1, column=1, sticky=W)
        # link to filename input to instance variable
        self.size["textvariable"] = self.curr_size

        # Date
        self.label_date = Label(self.form, text="Date:",
                                font=self.label_font)
        self.label_date.grid(row=2, column=0, sticky=NE)
        self.input_date = Label(self.form, justify="left")
        self.input_date.grid(row=2, column=1, sticky=W )
        # link to date input to instance variable
        self.input_date["textvariable"] = self.curr_date

        # Caption
        # give focus to keywords on TAB
        self.label_caption = Label(self.form, text="Caption:",
                                   font=self.label_font)
        self.label_caption.grid(row=3, column=0, sticky=NE)
        self.input_caption = Text(self.form, width=25,
                                  height=4, wrap=WORD)
        self.input_caption.grid(row=3, column=1)

        # Keywords
        self.label_keywords = Label(self.form, text="Keywords:",
                                    font=self.label_font)
        self.label_keywords.grid(row=5, column=0, sticky=NE)
        self.input_keywords = Text(self.form, width=25, height=2)
        self.input_keywords.grid(row=5, column=1)

        # Location
        self.label_location = Label(self.form, text="Location:",
                                    font=self.label_font)
        self.label_location.grid(row=6, column=0, sticky=NE)
        self.input_location = Entry(self.form, width=25)
        self.input_location.grid(row=6, column=1)
        # link to location entry variable
        self.input_location["textvariable"] = self.curr_location

        # People
        self.label_people = Label(self.form, text="People:",
                                  font=self.label_font)
        self.label_people.grid(row=7, column=0, sticky=NE)
        self.input_people = Entry(self.form, width=25)
        self.input_people.grid(row=7, column=1)
        # link to people entry variable
        self.input_people["textvariable"] = self.curr_people

        # Quality
        self.label_quality = Label(self.form, text="Quality:",
                                   font=self.label_font)
        self.label_quality.grid(row=8, column=0, sticky=NE)
        self.frame_quality = Frame(self.form, borderwidth=1,
                       relief=RIDGE)
        self.frame_quality.grid(row=8, column=1, sticky=W)
        Radiobutton(self.frame_quality, text="Excellent",
                    variable=self.curr_quality,
                    value=3).pack(anchor=W)
        Radiobutton(self.frame_quality, text="Good",
                    variable=self.curr_quality,
                    value=2).pack(anchor=W)
        Radiobutton(self.frame_quality, text="Average",
                    variable=self.curr_quality,
                    value=1).pack(anchor=W)
        Radiobutton(self.frame_quality, text="Poor",
                    variable=self.curr_quality,
                    value=0).pack(anchor=W)

        # Access
        self.label_access = Label(self.form, text="Access:",
                                  font=self.label_font)
        self.label_access.grid(row=9, column=0, sticky=NE)
        self.frame_access = Frame(self.form, borderwidth=1,
                                  relief=RIDGE)
        self.frame_access.grid(row=9, column=1, sticky=W)
        Radiobutton(self.frame_access, text="Public",
                    variable=self.curr_access,
                    value=2).pack(anchor=W)
        Radiobutton(self.frame_access, text="Personal",
                    variable=self.curr_access,
                    value=1).pack(anchor=W)
        Radiobutton(self.frame_access, text="Private",
                    variable=self.curr_access,
                    value=0).pack(anchor=W)

        # Photographer
        self.label_photographer = Label(self.form,
                                        text="Photographer:",
                                        font=self.label_font)
        self.label_photographer.grid(row=10, column=0, sticky=NE)
        self.input_photographer = Entry(self.form, width=25)
        self.input_photographer.grid(row=10, column=1)
        # link to photographer entry variable
        self.input_photographer["textvariable"] = self.curr_photographer

        # update button
        self.button_update = Button(self.form,
                                    command = lambda
                                    : self.update_button_click())
        self.button_update.configure(text="Update", width=6)
        self.button_update.grid(row=11, columnspan=2, sticky=NE)
        self.button_update.bind("<Return>",
                                lambda event
                                : self.update_button_click())

        # add to category button
        self.button_add2cat = Button(self.form,
                                     command = lambda
                                     : self.add2cat_button_click())
        self.button_add2cat.configure(text="Add to category", width=15)
        self.button_add2cat.grid(row=12, columnspan=2, sticky=E)
        self.button_add2cat.bind("<Return>",
                                 lambda event
                                 : self.add2cat_button_click())

        # add to export list
        self.button_add2export = Button(self.form,
                                        command = lambda
                                        : self.add2export_button_click())
        self.button_add2export.configure(text="Add to export list", width=15)
        self.button_add2export.grid(row=13, columnspan=2, sticky=E)
        self.button_add2export.bind("<Return>",
                                    lambda event
                                    : self.add2export_button_click())


        # display blank stuff
        self.display_blank_photo_info()

        # start polling categories listbox
        # must use polling since there is no variable that
        # contains the current listbox selection, only a method.
        self.curr_cat_index = None
        self.poll_cat()

        # start polling photos
        self.curr_photo_index = None
        self.poll_photos()

        # Extra keyboards bindings to allow for tab key on text boxes
        self.input_caption.bind("<Tab>", lambda event
                                : self.focus_to(self.input_keywords))
        self.input_keywords.bind("<Tab>", lambda event
                                 : self.focus_to(self.input_location))

        # Test RETURN binding needs a key command to move up and
        # down in photos to be useful
        self.input_people.bind("<Return>",
                               lambda event
                               : self.update_button_click())


    def focus_to(self, widget):
        """Moves focus to a particular widget, then breaks so that
        default binding is ignored"""
        # Would really like to do the following, but crashes and burns
        # event.widget.tk_focusNext().focus_set()
        widget.focus_set()
        return "break" # ignore default binding

    def old_select_category(self, event):
        """User has selected a category in the listbox"""
        """ If i uses curselection() here, it gives the last
        selection as it obviously hasn't updated the selection
        since the click event occurred. Shouldn't there be a
        better way? Associate with a widget modification event?"""

        # direct focus here
        self.input_cat.focus_set()

        # the dirty hack
        index = self.input_cat.nearest(event.y)

        # get ID of that index
        id = self.cat_index[index][0]

        # set current category - used for refresh
        self.current_cat = id

        # display photos and category info
        self.display_category(id)

    def select_photo(self, event):
        """User has just selected a photo from listbox"""

        # direct focus here
        self.input_photos.focus_set()

        # Dirty, dirty, dirty...
        index = self.input_photos.nearest(event.y)

        # get ID of that index
        id = self.photo_index[index][0]

        # display photo info
        self.display_photo_info(id)

    def update_thumbnail(self):
        filename = self.curr_filename.get()
        self.album.create_thumbnail(filename)

        #refresh display
        self.display_photo_info(filename)

    def poll_cat(self):
        """Repeatedly check the categories listbox for changes
        to the active item. If it has changed, then update the
        photos list"""
        # get the active entry from listbox
        # defaults to the first entry, 0
        index = self.input_cat.index(ACTIVE)

        # if changed since we last looked at it, update
        if (index != self.curr_cat_index
            and self.input_cat.curselection() != ()):
            # get ID of that index
            id = self.cat_index[index][0]

            # set current category - used for refresh
            self.current_cat = id

            # display photos and category info
            self.display_category(id)

            # display no thumbnail
            self.curr_photo_index = None
            self.display_blank_photo_info()

            self.curr_cat_index = index
        self.input_cat.after(self.listbox_poll_time,
                             self.poll_cat)

    def poll_photos(self):
        """Repeatedly check the photos listbox for changes to the
        active item. If changed, update photo info display"""
        # defaults to the first entry, 0
        index = self.input_photos.index(ACTIVE)

        if (index != self.curr_photo_index
            and self.input_photos.curselection() != ()):
            # get ID of that index
            id = self.photo_index[index][0]

            # display photo info
            self.display_photo_info(id)

            self.curr_photo_index = index
        self.input_photos.after(self.listbox_poll_time,
                                self.poll_photos)

    def view_photo(self, filename):
        if debug:
            print filename;
            print("%s %s/%s" % (self.image_viewer, Album.thumbnail_dir, filename))
        os.system("%s %s/%s" % (self.image_viewer, Album.thumbnail_dir, filename));

    def get_ids_from_selection(self):
        """Returns a list containing the ids of the selected
        images."""
        ids = []

        # FIX: error if none selected
        for index in self.input_photos.curselection():
            index = string.atoi(index)
            ids.append(self.photo_index[index][0])
        if debug:
            print ids
        return ids

    # ---------------
    # Display updates
    # ---------------
    def display_blank_photo_info(self):
        """No photo selected. Should probably also disable form
        input"""
        # Blank thumbnail
        self.thumb_image = Image.new('RGB',
                                     [self.thumb_display_width,
                                      self.thumb_display_height],
                                     'white')
        self.thumb_image = ImageTk.PhotoImage(self.thumb_image)
        self.thumb.configure(image=self.thumb_image)

        # blank everything else
        self.curr_date.set("")
        self.curr_filename.set("")
        self.curr_location.set("")
        self.curr_people.set("")
        self.curr_photographer.set("")
        self.curr_access.set("")
        self.curr_quality.set("")
        #self.curr_dims.set("") unused
        self.curr_size.set("")
        self.input_caption.delete(1.0,END)
        self.input_keywords.delete(1.0,END)

    def display_photo_info(self, id):
        # display in kilobytes
        conversion = 1024.0

        info = self.album.get_photo_info(id)

        # These labels/entries can be updated by just setting their instance var
        if debug:
            print id
        date = info.date
        self.curr_date.set(date)

        self.curr_filename.set(info.filename)
        self.curr_location.set(info.location)
        self.curr_people.set(info.people)
        self.curr_photographer.set(info.photographer)
        self.curr_access.set(info.access)
        self.curr_quality.set(info.quality)

        # Create dimensions and size string and update
        dims = str(info.xdim) + "x" + str(info.ydim)
        size = info.bytes / conversion
        self.curr_size.set("%s, %0.2f Kb" % (dims, size))

        # The caption & other texts are a pain to set
        self.input_caption.delete(1.0,END)
        if info.caption:
            self.input_caption.insert(END, info.caption)

        self.input_keywords.delete(1.0,END)
        if info.keywords:
            self.input_keywords.insert(END, info.keywords)

        # Display thumbnail in new thread
        thread.start_new_thread(self.display_thumbnail,
                    (self.curr_filename.get(),))

    def display_thumbnail(self, filename):
        """Displays/updates the thumbnail"""

        # Thumbnail
        self.image = Image.open("%s/%s" % (Album.thumbnail_dir, filename))

        self.image.thumbnail((self.thumb_display_width,
                      self.thumb_display_height),
                     self.thumb_display_filter)

        # Convert to PhotoImage
        self.image = ImageTk.PhotoImage(self.image)
        # Display it
        self.thumb.configure(image=self.image)

    def display_categories_list(self, listbox, special=False):
        """Display current categories in listbox"""
        # get categories from database - list of dictionaries
        categories = self.album.get_categories()

        # create categories list
        self.cat_index = []

        # FIX: depends on database internals

        if special:
            # entry for unfiled photos
            self.cat_index.append(["all", "All"])
            self.cat_index.append(["uncat", "Uncategorised"])

        for item in categories:
            self.cat_index.append([item['cat_id'], item['title']])

        listbox.delete(0,END) # Clear list

        # insert each category name into listbox
        for cat in self.cat_index:
            listbox.insert(END, cat[1])

        if special:
            # FIX: shouldn't display special categories in some lists
            # make special categories look nice
            listbox.itemconfig(0,bg="white")
            listbox.itemconfig(1,bg="white")

    def display_category(self, category_id):
        """Displays a listing of all the photos in a particular
        category and the information about that category."""
        # selected a special category
        if category_id == "uncat":
            # Uncategorised
            photos = self.album.get_uncategorised_photos()
            self.curr_cat_title.set("Uncategorised")
            self.curr_cat_subtitle.set("")
            self.curr_cat_desc.set("")

        elif category_id == "all":
            # All
            photos = self.album.get_all_photos()
            self.curr_cat_title.set("All")
            self.curr_cat_subtitle.set("")
            self.curr_cat_desc.set("")

        # selected a regular category
        else:
            photos = self.album.get_photos(category_id)

            # display category info
            info = self.album.get_cat_info(category_id)
            self.curr_cat_title.set(info[0]["title"])
            self.curr_cat_subtitle.set(info[0]["subtitle"])
            self.curr_cat_desc.set(info[0]["descr"])

        # create photos index
        self.photo_index = []

        for item in photos:
            # currently filename is both the index and display
            self.photo_index.append([item['filename'],
                                     item['filename']])

        self.input_photos.delete(0,END) # Clear list

        # insert each category name into listbox
        for photo in self.photo_index:
            self.input_photos.insert(END, photo[1])

    # -------------
    # Menu commands
    # -------------
    def export_original(self):
        # get IDs from photos listbox and check whether
        # things have been selected
        ids = self.get_ids_from_selection()
        self.album.export_original(ids)

    def export_category(self):
        self.album.export_category(self.current_cat)

    def rotate(self, angle):
        ids = self.get_ids_from_selection()
        self.album.rotate(angle, ids)

        #refresh display
        #fix this to re-display last selected photo, not the
        #first in the selection
        self.display_photo_info(ids[0])

    def slideshow(self):
        """Does a temporary export and calls a slideshow prog"""
        # FIX: create and remove temporary directory
        # FIX: cleanup looks nasty
        ids = self.get_ids_from_selection()

        # cycle through files
        for filename in ids:

            # Copy and resize image
            filepath = Album.photos_dir + "/" + filename
            savepath = self.category_dir + "/" + filename

            # copy original photo to 'originals'
            if debug:
                print "Copying original " + filename + " to " + savepath
            os.system("cp %s %s" % (filepath, savepath))

        # Slideshow!
        os.system(self.slideshow_viewer + " " + self.category_dir)

        # Cleanup
        for filename in ids:

            # Copy and resize image
            filepath = Album.photos_dir + "/" + filename
            savepath = self.category_dir + "/" + filename

            # copy original photo to 'originals'
            if debug:
                print "Copying original " + filename + " to " + savepath
            os.system("rm -f %s" % (savepath))

    def edit_image(self):
        # FIX: currently works on original image. Should create a
        # temporary copy and ask you if you want to replace
        # original with copy afterwards.
        filename = self.curr_filename.get()
        filepath = Album.photos_dir + "/" + filename
        #tempfile = "/tmp/edit_temp.jpg"
        #out = os.system("cp %s %s" % (filepath, tempfile))
        command = self.path_image_editor + " " + filepath
        thread.start_new_thread(os.system,(command,))
         # should pop up a dialog to say:
         # You are editing a temporary version of image.jpg. Choose update original when you have finished editing.
         # should then update thumbnail. this avoid the problem of editing in-place and the save-as directory listings taking forever due to large number of files

    def display_50_random_photos(self):
        photos = self.album.get_n_random_photos(50);
        print photos
        # create photos index
        self.photo_index = []

        for item in photos:
            # currently filename is both the index and display
            #self.photo_index.append(item)
            self.photo_index.append([item['filename'],
                                     item['filename']])

        self.input_photos.delete(0,END) # Clear list

        # insert each category name into listbox
        for photo in self.photo_index:
            self.input_photos.insert(END, photo[1])

    def remove_category(self):
        self.album.remove_category(self.current_cat)
        # update categories list
        self.display_categories_list(self.input_cat, special=True)
        # set current category - used for refresh
        self.current_cat = "uncat"
        # initially display uncategoriesed photos
        self.display_category("uncat")

    # -------
    # Dialogs
    # -------
    def dialog_import_dir_of_photos(self):
        """open up a file dialog to select a dir of photos"""
        # get directory
        batch_dir = tkFileDialog.askdirectory(initialdir=Album.add_default)

        self.album.import_dir_of_photos(batch_dir)

    def dialog_add_category(self):
        # open category dialog
        # allow selection between adding file to current category
        # and supplying info for new category
        top = Toplevel()

        form = Frame(top)
        form.pack()

        # Title
        Label(form, text="Title:").grid(row=0, column=0, sticky=E)
        input_title = Entry(form, justify="left")
        input_title.grid(row=0, column=1, sticky=W)

        # Subtitle
        Label(form, text="Subtitle:").grid(row=1, column=0, sticky=E)
        input_subtitle = Entry(form, justify="left")
        input_subtitle.grid(row=1, column=1, sticky=W)

        # Description
        Label(form, text="Description:").grid(row=2, column=0, sticky=E)
        input_desc = Text(form, width=25, height=4, wrap=WORD)
        input_desc.grid(row=2, column=1, sticky=W)

        # Create button
        button_create = Button(form, command = lambda
                               : self.create_cat_button_click(
                top,
                # pass the form values to the click command
                input_title.get().strip(string.whitespace),
                input_subtitle.get().strip(),
                input_desc.get(1.0, END)))

        button_create.configure(text="Create category")
        button_create.grid(row=3, columnspan=2, sticky=E)
        button_create.bind("<Return>", lambda event :
                               self.create_cat_button_click(
                top,
                # pass the form values to the click command
                input_title.get().strip(),
                input_subtitle.get().strip(),
                input_desc.get(1.0, END)))

        top.mainloop()

    def dialog_photos_by_sql(self):
        query = tkSimpleDialog.askstring("SQL query",
                                         "Enter SQL WHERE clause:",
                                         initialvalue="WHERE caption LIKE '%Ben%'")
        photos = self.album.get_photos_by_sql(query)

        # create photos index
        self.photo_index = []

        for item in photos:
            # currently filename is both the index and display
            self.photo_index.append([item['filename'],
                                     item['filename']])

        self.input_photos.delete(0,END) # Clear list

        # insert each category name into listbox
        for photo in self.photo_index:
            self.input_photos.insert(END, photo[1])

    # --------------
    # Button actions
    # --------------
    def choose_cat_button_click(self, dialog, listbox, photoIDs):
        """Choose button in "Add to category" dialog has just
        been clicked. Close the dialog, update the database and
        refresh the photos list"""
        index = string.atoi(listbox.curselection()[0])
        cat_id = self.cat_index[index][0]
        self.album.add_to_category(photoIDs, cat_id)

        #close dialog
        dialog.destroy()

        # this doesn't seem to be working...
        self.display_category(self.current_cat)

    def create_cat_button_click(self, dialog, title,
                                subtitle, desc):
        """create button in Add category dialog has just been
        clicked"""
        self.album.add_category(title, subtitle, desc)
        # close dialog
        dialog.destroy()
        self.display_categories_list(self.input_cat, special=True)

    def add2cat_button_click(self):
        ids = self.get_ids_from_selection()

        # popup dialog to select category
        top = Toplevel()

        #### Categories window ####
        # look at tixHList if implementing hierachial categories
        frame_cat = Frame(top)
        frame_cat.pack(side=LEFT)

        # scrollbar for listbox
        scrollbar = Scrollbar(frame_cat)
        scrollbar.pack(side=RIGHT, fill=Y)

        # export selection allows having items selected in
        # multiple list boxes at once
        input_cat = Listbox(frame_cat,
                            width=30, height=15,
                            # ONLY ONE CATEGORY
                            selectmode=SINGLE,
                            exportselection=0,
                            # inform scrollbar of list events
                            yscrollcommand=scrollbar.set)

        # inform listbox of scrollbar events
        scrollbar.config(command=input_cat.yview)

        # update categories list
        self.display_categories_list(input_cat)

        input_cat.pack()

        # Create button
        button_choose = Button(frame_cat, command = lambda
                               : self.choose_cat_button_click(
                top,
                # pass selection to the click command
                input_cat, ids))

        button_choose.configure(text="Choose")
        button_choose.pack()
        button_choose.bind("<Return>", lambda event :
                               self.choose_cat_button_click(
                top,
                # pass selection to the click command
                input_cat, ids))

        top.mainloop()

        # update incat table in database
        # reload current photos list

    def add2export_button_click(self):
        ids = self.get_ids_from_selection()

        export_file = open('/home/ben/photos/export/export-list.txt', 'a');

        for id in ids:
            cat_title = self.album.db.cat_title_of_photo(id);
            print(cat_title)
            export_file.write(id + ',' + cat_title + '\n');

        export_file.close()


    def update_button_click(self):
        # update database - don't need dimensions or filesize
        ### should check that a photo is selected
        #date = self.curr_date.get().strip()
        location = self.curr_location.get().strip()
        people = self.curr_people.get().strip()
        photographer = self.curr_photographer.get().strip()
        caption = self.input_caption.get(1.0,END).strip()
        keywords = self.input_keywords.get(1.0,END).strip()

        self.album.update_photo_info(self.curr_filename.get(),
                                     caption, keywords,
                                     location, people, photographer,
                                     self.curr_quality.get(),
                                     self.curr_access.get())

    # ---------
    # Galleries
    # ---------
    def make_web_gallery(self):
        category_dir = tkSimpleDialog.askstring("Web Gallery",
                                                "Gallery folder",
                                                parent = self.myParent)

        title = tkSimpleDialog.askstring("Web Gallery",
                                         "Page headline",
                                         parent = self.myParent)
        blurb = tkSimpleDialog.askstring("Web Gallery",
                                         "Blurb",
                                         parent = self.myParent)

        # get selection of images
        ids = self.get_ids_from_selection()

        self.album.make_web_gallery(ids, category_dir, title, blurb)

def main():
    root = Tk()
    root.wm_title(Gui.TITLE)
    root.geometry(Gui.GEOMETRY_DEFAULT)
    root.option_add( "*font", "helvetica 11" )
    myapp = Gui(root)
    root.mainloop()
