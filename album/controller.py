"""
Photo album: A tool to help photographers manage a large amount
of digital images.

Ben Sturmfels

Requires:
See README for requirements.

TODO:
 - update to use exiftran
 - import log
 - add an import date
look at gwhere for cataloging CDs of saved photos
album stats - # images, total size, earliest photo, latest photo
category stats - create date
use wxpython instead of tkinter - guido rekons it's the way to go
derive dialogs from tkSimpleDialog.Dialog
uncategoried doesn't refresh after add images
Adding a category doesn't refresh the categories list properly - ids are wrong
stop spaces being quoted in category details when adding new cats
empty values should be stored as NULL - NULL values should be displayed as empty
fix date parsing
work on functions that write to new database
photo diary - calendar with photos each day
simply code to display a set of photos - code duplication sql, random
Photos list displays thumbnails
Be able to modify date of images
Doesn't refresh uncategorised list after import
Partial functions http://www.python.org/peps/pep-0309.html
Fix slideshow
Local configuration file
Setup
Exception handling
Any operations on the original should check the digest first
Lazy updating of image thumbnail if older than original
Remove from category button
Fix non-null requirement on database fields
Thumbnail is fixed size - window shouldn't jump around
Progress display while adding photos
Better list than photo filenames - thumbs? caption?
Update category details
Update on change photo? prolly also on <RETURN> in any field
Move should be last thing done on add photo
Default active selection ALL
Title in web gallery maker
"""

import cgi
import fnmatch
import glob
import os
import random
import re
import string
import time

import ExifTags
import Image

# My modules
import db
import photoinfo

debug = True

class Album:
    # Default settings
    thumb_storage_width = 400
    thumb_storage_height = 400
    thumb_jpg_quality = 85

    add_default = "/home/ben/photos/download"
    web_gallery_dir = "/home/ben/photos/gallery"
    web_templates_dir = "/home/ben/album/templates"

    default_quality = 1 # average
    default_access = 2 # public
    default_photographer = "Ben"
    # storage_id defaults to null
    default_equip = "Canon PowerShot A70"
    path_jpegtran = "jpegtran"

    photos_dir = "/home/ben/album/photos"
    thumbnail_dir = "/home/ben/album/thumbs"
    export_dir = "/home/ben/photos/export"


    def __init__(self):
        # Fix: read in configuration to replace default settings

        # FIX: directories exist for photos and thumbnails?
        # if not create them

        # probably shouldn't be a class variable
        self.db = db.DB()

    # ----------------------------
    # Get photo or categories info
    # ----------------------------
    def get_photo_info(self, id):
        '''Grabs all the info about a photo and returns it as
        a dictionary'''
        result = self.db.get_photo_info(id)
        if debug:
            print result

        return photoinfo.PhotoInfo(result[0]["filename"],
                                   result[0]["x_dim"],
                                   result[0]["y_dim"],
                                   result[0]["bytes"],
                                   result[0]["date"],
                                   result[0]["caption"],
                                   result[0]["keywords"],
                                   result[0]["location"],
                                   result[0]["people"],
                                   result[0]["photographer"],
                                   result[0]["access"],
                                   result[0]["quality"])

    def get_categories(self):
        return self.db.get_categories()

    def get_uncategorised_photos(self):
        return self.db.get_uncategorised_photos()

    def get_all_photos(self):
        return  self.db.get_all_photos()

    def get_photos(self, id):
        return self.db.get_photos(id)

    def get_photos_by_sql(self, clause):
        return self.db.get_photos_by_sql(clause)

    def get_cat_info(self, id):
        return self.db.get_cat_info(id)

    def get_n_random_photos(self, n):
        "Generate a list of n random photos."
        random.seed()

        # generate n random indexes within our range of photos
        indexes = [random.randrange(0, self.db.count_photos)
                   for m in range (0, n)]

        # map indexes to photos
        all_photos = self.get_all_photos()
        return [all_photos[i] for i in indexes]

    # ------------------------
    # Add photos or categories
    # ------------------------
    def add_category(self, title, subtitle, desc):
        # update database
        self.db.add_category(title, subtitle, desc)

    def import_dir_of_photos(self, batch_dir):
        """Given a directory, each image in the directory is
        added to the album (uncategorised) and the original file
        is moved to the album folder."""

        # FIX: give this a recursive option

        # retreives list of images given directory and cycles
        # through each one
        # FIX: check that batch_dir exists and is readable
        files = glob.glob("%s/*" % (batch_dir))
        for file in files:
            # screen out jpg files - case insensitive
            if not fnmatch.fnmatch(file.lower(), "*.jpg"):
                continue
            try:
                # Open image
                image = Image.open(file)
            except IOError, e:
                print "Unable to open '%s': " % (file), e
                break

            # Does it have EXIF data?
            tags = image._getexif()
            use_mtime = False
            if tags != None:
                # Map raw EXIF data to nice names
                named_tags = {}
                for key in tags.keys():
                    if ExifTags.TAGS.has_key(key):
                        named_tags[ExifTags.TAGS[key]]= tags[key]

                # get date-time
                if (named_tags.has_key("DateTime")
                    and named_tags["DateTime"] != "0000:00:00 00:00:00"):
                    datetime = named_tags["DateTime"]
                    date_prefix = re.sub(":", "-", datetime)
                    date_prefix = re.sub(" ", "+", date_prefix)

                elif (named_tags.has_key("DateTimeOriginal")
                      and named_tags["DateTime"] != "0000:00:00 00:00:00"):
                    datetime = named_tags["DateTimeOriginal"]
                    date_prefix = re.sub(":", "-", datetime)
                    date_prefix = re.sub(" ", "+", date_prefix)

                # No EXIF data - use modification date-time. 'm' in
                # filename indicates date is from modification
                else: use_mtime = True

            else: use_mtime = True

            if use_mtime:
                # create a filename based on the modification time
                if debug:
                    print "Can't read EXIF datetime, modification date used for %s" \
                        % (file)
                mtime = os.path.getmtime(file)
                mtime = time.gmtime(mtime)
                datetime = time.strftime("%Y:%m:%d %H:%M:%S", mtime)
                date_prefix = time.strftime("%Y-%m-%dm%H-%M-%S", mtime)

            # get image dimensions
            width = image.size[0]
            height = image.size[1]

            # get file size
            bytes = os.path.getsize(file)

            # include directory in name
            # eg. /mydir/2004-02-01+09:34:02
            file_prefix = "%s/%s" % (self.photos_dir, date_prefix)
            print file_prefix
            # Allow for multiple images taken within the same
            # second (with EXIF data) or multiple images with
            # same modification date-time. These images are given
            # an extra number on the end of the filename,
            # eg. ####-##-##-1.jpg, ####-##-##-2.jpg
            if os.path.isfile("%s-1.jpg" % (file_prefix)):
                if debug:
                    print "Have at least 2 in series of photos..."
                count = 2
                while os.path.isfile("%s-%s.jpg" % (file_prefix, count)):
                    count = count + 1
                new_file = "%s-%s.jpg" % (date_prefix, count)

            # already have one with same time
            elif os.path.isfile("%s.jpg" % (file_prefix)):
                if debug:
                    print "Have one with same filename"
                # rename the existing as 1st in series
                os.rename("%s.jpg" % (file_prefix), "%s-1.jpg" % (file_prefix))
                # rename thumbnail
                os.rename("%s/%s.jpg" % (self.thumbnail_dir,date_prefix),
                          "%s/%s-1.jpg" % (self.thumbnail_dir,date_prefix))
                # update filename in database
                self.db.update_filename("%s.jpg" % (date_prefix),
                                        "%s-1.jpg" % (date_prefix))
                if debug:
                    print "rename existing %s.jpg to %s-1.jpg" \
                        % (file_prefix, file_prefix)
                # rename current file to 2nd in series
                new_file = "%s-2.jpg" % (date_prefix)
            # filename is original
            else:
                new_file = "%s.jpg" % (date_prefix)

            # FIX: check file is not already in database

            # Move file to its new home in the album directory
            if debug:
                print "rename to %s/%s" % (self.photos_dir,new_file)
            os.rename(file, "%s/%s" % (self.photos_dir,new_file))

            # create thumbnail
            self.create_thumbnail(new_file)

            # add file to database
            self.db.add_photo(new_file, datetime, width, height,
                              bytes, self.default_quality,
                              self.default_access,
                              self.default_photographer,
                              self.default_equip)
            # FIX: check success

    def add_to_category(self, photo_ids, category_id):
        self.db.add_to_category(photo_ids, category_id)

    # ------------------------------
    # Update photo or category info
    # ------------------------------
    def update_photo_info(self, filename, caption, keywords,
                          location, people, photographer,
                          quality, access):
        """Update's the database information about a photo"""
        self.db.update_photo_info(filename, caption, keywords,
                                  location, people, photographer,
                                  quality, access)

    def remove_category(self, id):
        self.db.remove_category(id)

    # -------------------------
    # Modify photo or thumbnail
    # -------------------------
    def create_thumbnail(self, filename):
        """Used when adding a new image or when an existing image
        has been modified and thumbnail needs to be updated."""
        filepath = self.photos_dir + "/" + filename
        image = Image.open(filepath)
        # get new pixel size from full image
        width = image.size[0]
        height = image.size[1]
        # get file size
        bytes = os.path.getsize(filepath)

        # recreate thumbnail
        if debug:
            print("Creating new thumbnail...")
        image.thumbnail((self.thumb_storage_width,
                         self.thumb_storage_height),
                        Image.ANTIALIAS)
        image.save("%s/%s" % (self.thumbnail_dir,filename),
                   quality = self.thumb_jpg_quality)

        # update file info in database
        self.db.update_sizes(filename, width, height, bytes)

        # FIX: check thumbnail creation & database update
        # succeeded

    def rotate(self, angle, ids):
        """Losslessly rotate selected JPEG images through the the given
        angle(degrees) clockwise, keeping EXIF data."""

        # FIX: test that file is stored locally eg. not on DVD

        # cycle through files
        for filename in ids:
            # Rotate image
            filepath = self.photos_dir + "/" + filename
            tempfile = "/tmp/rot_temp.jpg"
            out = os.system("%s -copy all -rotate %d %s > %s" % \
                                (self.path_jpegtran, angle, filepath, tempfile))
            if debug:
                print out
            if out == 0: # Rotation was successful
                # replace original with temp
                os.system("mv %s %s" % (tempfile, filepath))

                # get new pixel size and filesize from full image
                image = Image.open(filepath)
                width = image.size[0]
                height = image.size[1]
                bytes = os.path.getsize(filepath)
                # update file info in database
                self.db.update_sizes(filename, width, height, bytes)

                # recreate thumbnail
                self.create_thumbnail(filename)

    # ---------------
    # Export & backup
    # ---------------
    def export_original(self, ids):
        """Copies the original to the gallery folder"""

        # Cycle through files
        for filename in ids:
            # Copy and resize image
            filepath = Album.photos_dir + "/" + filename
            savepath = self.export_dir + "/" + filename

            # Copy original photo to 'originals'
            if debug:
                print "Copying original " + filename + " to " + savepath
            os.system("cp %s %s" % (filepath, savepath))

    def export_category(self, id):
        """Exports the whole category as a folder"""
        # make directory
        info = self.get_cat_info(id)
        dirname = self.export_dir + "/" + info[0]["title"]
        os.system("mkdir \"%s\"" % (dirname))

        # export files
        files = []
        for item in self.db.get_photos(id):
            files.append(item["filename"])

        # grr... copying part of export_original
                # Cycle through files
        for filename in files:
            # Copy and resize image
            filepath = Album.photos_dir + "/" + filename
            savepath = dirname + "/" + filename

            # Copy original photo to 'originals'
            if debug:
                print "Copying original " + filename + " to " + savepath
            os.system("cp \"%s\" \"%s\"" % (filepath, savepath))


    # ---------
    # Galleries
    # ---------
    def make_web_gallery(self, ids, category_dir, title,
                          blurb):
        """Creates a web gallery"""
        ### header template always contains same title - fix

        photo_width = 640
        photo_height = 640
        thumb_width = 600 # we don't want the width to limit height
        thumb_height = 120
        photo_quality = 90
        thumb_quality = 85


        # set up required directories
        os.makedirs("%s/%s/images" % (self.web_gallery_dir, category_dir))

        gallery = "%s/%s" % (self.web_gallery_dir, category_dir)

        # open output HTML page
        page = open(gallery + "/index.php", "w")

        # dump in header
        header = open("%s/gallery_header" % (self.web_templates_dir), "r")
        page.writelines(header.readlines())
        header.close()

        page.write("<h1>%s</h1>\n<p>%s</p>\n" % (title, blurb))
        page.write("<div class=\"gallery\">\n")

        # cycle through files
        for filename in ids:
            image = Image.open(self.photos_dir + "/" + filename)
            fullpath = "images/" + filename
            thumbpath = "images/thumb-" + filename

            # create thumbnail & normal sized image
            if debug:
                print("Creating new thumbnail...")
            fullsavepath = """%s/%s/%s""" % (self.web_gallery_dir,
                                             category_dir,
                                             fullpath)
            thumbsavepath = """%s/%s/%s""" % (self.web_gallery_dir,
                                              category_dir,
                                              thumbpath)
            # create regular
            image.thumbnail((photo_width, photo_height), Image.ANTIALIAS)
            image.save(fullsavepath, quality = photo_quality)

            # create thumbnail
            image.thumbnail((thumb_width, thumb_height), Image.ANTIALIAS)
            image.save(thumbsavepath, quality = thumb_quality)

            # get info from database
            info = self.db.get_photo_info(filename)
            caption = cgi.escape(info[0]["caption"])

            width=image.size[0]
            height=image.size[1]

            # ouput HTML for each image
            line = """<div class=\"photo\">
<a href=\"%s\"><img src=\"%s\" width=\"%s\" height=\"%s\" alt=\"\" /></a>
<p>%s</p>
</div>
""" % (fullpath, thumbpath, width, height, caption)
            page.write(line)

        # close gallery
        page.write("</div>\n")

        # dump in footer
        footer = open("%s/gallery_footer" % (self.web_templates_dir), "r")
        page.writelines(footer.readlines())
        footer.close()

        # close file
        page.close()
