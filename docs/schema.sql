CREATE TABLE cat (
	cat_id INTEGER PRIMARY KEY,
	title TEXT NOT NULL,
	subtitle TEXT,
	descr TEXT
);

CREATE TABLE in_cat (
  filename TEXT NOT NULL,
  cat_id INT NOT NULL
);

CREATE TABLE photo (
  filename TEXT PRIMARY KEY NOT NULL,
  md5 TEXT,
  date TEXT,
  x_dim INTEGER NOT NULL,
  y_dim INTEGER NOT NULL,
  bytes INTEGER NOT NULL,
  caption TEXT,
  keywords TEXT,
  location TEXT,
  people TEXT,
  access INTEGER NOT NULL,
  quality INTEGER NOT NULL,
  photographer TEXT NOT NULL,
  equip TEXT,
  film TEXT,
  storage_id INTEGER,
  uncat INT
);
